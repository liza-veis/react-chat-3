export * from './colors';

export const CHAT_NAME = 'ReactChat';
export const MESSAGES_URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';
export const LAST_MESSAGE_DATE_FORMAT = 'DD.MM.YYYY HH:mm';
export const MESSAGE_LIST_DATE_FORMAT = 'dddd, DD MMMM';
export const MESSAGE_TIME_FORMAT = 'HH:mm';
export const EDIT_LAST_MESSAGE_KEY = 'ArrowUp';

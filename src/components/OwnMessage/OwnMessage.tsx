import { FC } from 'react';
import { AiOutlineEdit, AiOutlineDelete } from 'react-icons/ai';
import { useDispatch } from 'react-redux';
import { MESSAGE_TIME_FORMAT } from '../../constants';
import { formatDate } from '../../helpers';
import { OwnMessageData } from '../../types';
import { deleteMessage, setEditedMessage } from '../Chat/chatActions';
import * as S from './styled';

type OwnMessageProps = {
  data: OwnMessageData;
};

export const OwnMessage: FC<OwnMessageProps> = ({ data }) => {
  const dispatch = useDispatch();

  const { text, createdAt, id } = data;
  const time = formatDate(createdAt, MESSAGE_TIME_FORMAT);

  const handleDeleteMessage = () => dispatch(deleteMessage(id));

  const handleEditMessage = () => dispatch(setEditedMessage(id));

  return (
    <div className="own-message">
      <S.MessageButtonsContainer>
        <S.MessageButton className="message-edit" title="Edit" onClick={handleEditMessage}>
          <AiOutlineEdit />
        </S.MessageButton>
        <S.MessageButton className="message-delete" title="Delete" onClick={handleDeleteMessage}>
          <AiOutlineDelete />
        </S.MessageButton>
      </S.MessageButtonsContainer>
      <S.MessageContainer>
        <S.MessageAside className="message-aside">
          <S.MessageTime className="message-time">{time}</S.MessageTime>
        </S.MessageAside>
        <S.MessagesContent className="message-content">
          <S.MessageText className="message-text">{text}</S.MessageText>
        </S.MessagesContent>
      </S.MessageContainer>
    </div>
  );
};

import styled from 'styled-components';
import { COLOR_ACCENT, COLOR_FONT_GREY, COLOR_WHITE } from '../../constants';

export const MessageButtonsContainer = styled.div`
  flex-basis: 100%;
  display: flex;
  gap: 5px;
  align-items: center;
  justify-content: flex-end;
  margin-bottom: 5px;
`;

export const MessageContainer = styled.div`
  display: flex;
  align-items: flex-end;
  max-width: calc(50% - 100px);
  gap: 10px;
  margin-bottom: 20px;
  margin-left: auto;
  justify-content: flex-end;
`;

export const MessagesContent = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 5px 15px 10px;
  min-width: 150px;
  background-color: ${COLOR_ACCENT};
  color: ${COLOR_WHITE};
  border-radius: 10px 10px 0px;
`;

export const MessageText = styled.span`
  padding: 0;
`;

export const MessageAside = styled.div`
  align-self: flex-start;
  flex-shrink: 0;
  text-align: right;
`;

export const MessageTime = styled.span`
  font-size: 14px;
  color: ${COLOR_FONT_GREY};
  line-height: 1;
`;

export const MessageButton = styled.button`
  display: flex;
  font-size: 20px;
  color: ${COLOR_ACCENT};
  background-color: transparent;
  border: none;
  cursor: pointer;
  outline: none;
`;

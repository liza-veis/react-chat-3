import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CHAT_NAME, EDIT_LAST_MESSAGE_KEY, LAST_MESSAGE_DATE_FORMAT } from '../../constants';
import { formatDate, getLastMessageDate, getUsersCount } from '../../helpers';
import { getLastOwnMessage } from '../../helpers/getLastOwnMessage';
import { useFetch, useKeyup } from '../../hooks';
import { MessageData } from '../../types';
import { EditModal } from '../EditModal';
import { Header } from '../Header';
import { MessageInput } from '../MessageInput';
import { MessageList } from '../MessageList';
import { Preloader } from '../Preloader';
import { setEditedMessage, setMessages, setPreloader } from './chatActions';
import { selectEditedMessageId, selectMessages, selectPreloader } from './chatSelectors';
import * as S from './styled';

type ChatProps = {
  url: string;
};

export const Chat: FC<ChatProps> = ({ url }) => {
  const dispatch = useDispatch();
  const messages = useSelector(selectMessages);
  const preloader = useSelector(selectPreloader);
  const editedMessageId = useSelector(selectEditedMessageId);

  const { data, isLoading } = useFetch<MessageData[]>(url);

  const lastMessageDate = getLastMessageDate(messages);

  const headerData = {
    messagesCount: messages.length,
    usersCount: getUsersCount(messages),
    lastMessageDate: formatDate(lastMessageDate, LAST_MESSAGE_DATE_FORMAT),
  };

  const editLastMessage = () => {
    if (editedMessageId) return;

    const lastMessage = getLastOwnMessage(messages);
    if (lastMessage) {
      dispatch(setEditedMessage(lastMessage.id));
    }
  };

  useKeyup(EDIT_LAST_MESSAGE_KEY, editLastMessage);

  useEffect(() => {
    dispatch(setPreloader(isLoading));
  }, [dispatch, isLoading]);

  useEffect(() => {
    if (data) dispatch(setMessages(data));
  }, [dispatch, data]);

  return (
    <S.Chat className="chat">
      <S.GlobalStyle />
      <S.Container>
        {preloader && <Preloader />}
        {!preloader && (
          <>
            <Header title={CHAT_NAME} {...headerData} />
            <MessageList messages={messages} />
            <MessageInput />
          </>
        )}
      </S.Container>
      <EditModal />
    </S.Chat>
  );
};

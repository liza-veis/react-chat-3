import { AnyAction } from 'redux';
import { ChatState } from '../../types';
import * as actionTypes from './chatActionTypes';

const initialState: ChatState = {
  messages: [],
  editModal: false,
  preloader: true,
  editedMessageId: null,
};

export const chatReducer = (state = initialState, action: AnyAction): ChatState => {
  switch (action.type) {
    case actionTypes.SET_MESSAGES: {
      const { payload } = action;
      return { ...state, messages: payload };
    }

    case actionTypes.SET_PRELOADER: {
      const { payload } = action;
      return { ...state, preloader: payload };
    }

    case actionTypes.ADD_MESSAGE: {
      const { payload } = action;
      const { messages } = state;
      return { ...state, messages: messages.concat(payload) };
    }

    case actionTypes.SET_MESSAGE: {
      const { payload } = action;
      const messages = state.messages.map((message) =>
        message.id === payload.id ? payload : message
      );
      return { ...state, messages };
    }

    case actionTypes.DELETE_MESSAGE: {
      const { payload } = action;
      const messages = state.messages.filter(({ id }) => id !== payload);
      return { ...state, messages };
    }

    case actionTypes.SET_EDITED_MESSAGE: {
      const { payload } = action;
      return { ...state, editedMessageId: payload, editModal: true };
    }

    case actionTypes.RESET_EDITED_MESSAGE: {
      return { ...state, editedMessageId: null, editModal: false };
    }
  }

  return state;
};

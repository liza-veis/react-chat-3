import { FC } from 'react';
import * as S from './styled';

type HeaderProps = {
  title: string;
  usersCount: number;
  messagesCount: number;
  lastMessageDate: string;
};

export const Header: FC<HeaderProps> = ({ title, usersCount, messagesCount, lastMessageDate }) => {
  return (
    <S.Header className="header">
      <S.HeaderTitle className="header-title">{title}</S.HeaderTitle>
      <S.UsersCount className="header-users-count">{usersCount}</S.UsersCount>
      <S.MessagesCount className="header-messages-count">{messagesCount}</S.MessagesCount>
      <S.LastMessageDate className="header-last-message-date">{lastMessageDate}</S.LastMessageDate>
    </S.Header>
  );
};

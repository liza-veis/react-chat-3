import { FC } from 'react';
import { MESSAGES_URL } from '../../constants';
import { Chat } from '../Chat';

export const App: FC = () => {
  return (
    <div className="App">
      <Chat url={MESSAGES_URL} />
    </div>
  );
};

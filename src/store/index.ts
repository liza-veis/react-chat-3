import { combineReducers, createStore } from 'redux';
import { chatReducer } from '../components/Chat/chatReducer';

export const rootReducer = combineReducers({
  chat: chatReducer,
});

export const store = createStore(rootReducer);

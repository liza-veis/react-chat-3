import { store } from './store';

export type MessageData = {
  id: string;
  userId: string;
  avatar: string;
  user: string;
  text: string;
  createdAt: string;
  editedAt: string;
};

export type OwnMessageData = {
  id: string;
  text: string;
  createdAt: string;
};

export type AnyMessageData = MessageData | OwnMessageData;

export type SplitMessagesData = {
  [date: string]: (MessageData | OwnMessageData)[];
};

export type RootState = ReturnType<typeof store.getState>;

export type ChatState = {
  messages: AnyMessageData[];
  editModal: boolean;
  preloader: boolean;
  editedMessageId: string | null;
};

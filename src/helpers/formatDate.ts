import moment from 'moment';

export const formatDate = (date: string, format: string) => moment(new Date(date)).format(format);

import { AnyMessageData } from '../types';

export const sortMessagesByDate = (messages: AnyMessageData[]) =>
  messages.slice().sort((a, b) => +new Date(a.createdAt) - +new Date(b.createdAt));
